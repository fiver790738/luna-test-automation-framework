package com.magento.assignment.utils;

public class Constants {

    public final static String VALID = "valid";
    public final static String INVALID = "invalid";
    public final static String EMPTY = "empty";

    public final static String ERROR_MESSAGE = "Value type should be valid, invalid or empty!";
    public final static String LOGIN_ERROR_MESSAGE = "The account sign-in was incorrect or your account is disabled temporarily. Please wait and try again later.";
    public final static String RESET_PASS_SUCCESS_MESSAGE = "If there is an account associated with %s you will receive an email with a link to reset your password.";
}

package com.magento.assignment.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ForgotPasswordPage {

    private final SelenideElement pageTitle = $x("//div[@class='page-title-wrapper']");
    private final SelenideElement noteLabel = $x("//div[@class='field note']");

    private final SelenideElement emailFieldLabel = $x("//label[@for='email_address']");
    private final SelenideElement emailInputField = $x("//input[@name='email']");
    private final SelenideElement emailFieldErrorLabel = $x("//div[@id='email_address-error']");

    private final SelenideElement resetPasswordButton = $x("//button[@class='action submit primary']");

    public boolean isPageTitleDisplayed() {
        return pageTitle.isDisplayed();
    }

    public String getPageTitleText() {
        return pageTitle.getText();
    }

    public boolean isNoteLabelDisplayed() {
        return noteLabel.isDisplayed();
    }

    public String getNoteLabelText() {
        return noteLabel.getText();
    }

    public String getEmailFieldLabelText() {
        return emailFieldLabel.getText();
    }

    public boolean isEmailInputFieldDisplayed() {
        return emailInputField.isDisplayed();
    }

    public void enterValueToEmailInputField(String value) {
        emailInputField.sendKeys(value);
    }

    public boolean isEmailFieldErrorLabelDisplayed() {
        return emailFieldErrorLabel.isDisplayed();
    }

    public String getEmailFieldErrorLabelText() {
        return emailFieldErrorLabel.getText();
    }

    public boolean isResetPasswordButtonDisplayed() {
        return resetPasswordButton.isDisplayed();
    }

    public String getResetPasswordButtonText() {
        return resetPasswordButton.getText();
    }

    public void clickOnResetPasswordButton() {
        resetPasswordButton.click();
    }
}

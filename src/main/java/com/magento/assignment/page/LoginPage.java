package com.magento.assignment.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {

    private final SelenideElement pageTitle = $x("//h1[@class='page-title']");
    private final SelenideElement loginHeadingLabel = $x("//div[@class='login-container']//strong[@id='block-customer-login-heading']");
    private final SelenideElement noteLabel = $x("//div[@class='field note']");
    private final SelenideElement alertLabel = $x("//div[@role='alert']");

    private final SelenideElement emailFieldLabel = $x("//label[@for='email']");
    private final SelenideElement emailInputField = $x("//input[@id='email']");
    private final SelenideElement passwordFieldLabel = $x("//div[@class='login-container']//label[@for='pass']");
    private final SelenideElement passwordInputField = $x("//div[@class='login-container']//input[@id='pass']");
    private final ElementsCollection requiredFieldErrorLabels = $$x("//div[@class='mage-error']");

    private final SelenideElement signInButton = $x("//button[@class='action login primary']");
    private final SelenideElement forgotPasswordButton = $x("//a[@class='action remind']");

    public boolean isPageTitleDisplayed() {
        return pageTitle.isDisplayed();
    }

    public String getPageTitleText() {
        return pageTitle.getText();
    }

    public boolean isLoginHeadingLabelDisplayed() {
        return loginHeadingLabel.isDisplayed();
    }

    public String getLoginHeadingLabelText() {
        return loginHeadingLabel.getText();
    }

    public boolean isNoteLabelDisplayed() {
        return noteLabel.isDisplayed();
    }

    public String getNoteLabelText() {
        return noteLabel.getText();
    }

    public boolean isAlertLabelDisplayed() {
        return alertLabel.isDisplayed();
    }

    public String getAlertLabelText(boolean isSuccess) {
        if (isSuccess) {
            alertLabel.shouldHave(Condition.text("If there is an account"));
        } else {
            alertLabel.shouldHave(Condition.text("incorrect"));
        }
        return alertLabel.getText();
    }

    public String getEmailFieldLabelText() {
        return emailFieldLabel.getText();
    }

    public boolean isEmailInputFieldDisplayed() {
        return emailInputField.isDisplayed();
    }

    public void enterValueToEmailInputField(String value) {
        emailInputField.sendKeys(value);
    }

    public String getPasswordFieldLabelText() {
        return passwordFieldLabel.getText();
    }

    public boolean isPasswordFieldLabelDisplayed() {
        return passwordFieldLabel.isDisplayed();
    }

    public void enterValueToPasswordInputField(String value) {
        passwordInputField.sendKeys(value);
    }

    public boolean verifyRequiredFieldErrorLabels() {
        for (SelenideElement element : requiredFieldErrorLabels) {
            if (!element.isDisplayed()) {
                return false;
            }
            if (!element.getText().equals("This is a required field.")) {
                return false;
            }
        }
        return true;
    }

    public boolean isSignInButtonDisplayed() {
        return signInButton.isDisplayed();
    }

    public String getSignInButtonText() {
        return signInButton.getText();
    }

    public void clickOnSignInButton() {
        signInButton.click();
    }

    public boolean isForgotPasswordButtonDisplayed() {
        return forgotPasswordButton.isDisplayed();
    }

    public String getForgotPasswordButtonText() {
        return forgotPasswordButton.getText();
    }

    public void clickOnForgotPasswordButton() {
        forgotPasswordButton.click();
    }
}

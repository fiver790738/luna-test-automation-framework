package com.magento.assignment.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class HomePage {

    private final SelenideElement signInButton = $x("//div[@class='panel header']//li[@class='authorization-link']");

    private final SelenideElement userLoggedInLabel = $x("//div[@class='panel header']/ul[@class='header links']");

    private final SelenideElement customerMenuButton = $x("(//button[@data-action='customer-menu-toggle'])[1]");
    private final SelenideElement signOutButton = $x("(//a[contains(@href, 'logout')])[1]");

    public boolean isSignInButtonDisplayed() {
        return signInButton.isDisplayed();
    }

    public void clickOnSignInButton() {
        signInButton.click();
    }

    public boolean isUserLoggedInLabelDisplayed() {
        return userLoggedInLabel.isDisplayed();
    }

    public String getUserLoggedInLabelText() {
        userLoggedInLabel.shouldHave(Condition.text("Welcome,"));
        return userLoggedInLabel.getText().split("\n")[0];
    }

    public void clickOnCustomerMenuButton() {
        customerMenuButton.click();
    }

    public void clickOnSignOutButton() {
        signOutButton.click();
    }
}

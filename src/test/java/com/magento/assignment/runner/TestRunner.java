package com.magento.assignment.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"classpath:features"},
        tags = {"not @Ignore"},
        glue = {"com.magento.assignment.steps", "com.magento.assignment.config", "com.magento.assignment.hooks"},
        plugin = {
                "pretty",
                "html:target/site/cucumber-reports",
                "json:target/cucumber.json",
                "com.magento.assignment.steps.TestStepWatch",
                "com.magento.assignment.steps.LifecycleSteps"
        })
public class TestRunner {
}

package com.magento.assignment.steps;

import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.EventHandler;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestStepStarted;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestStepWatch implements ConcurrentEventListener {
    private static final String DEVIDER =
            "===================================================================================";

    private final EventHandler<TestStepStarted> stepHandler =
            new EventHandler<>() {
                @Override
                public void receive(TestStepStarted event) {
                    handleTestStep(event);
                }

                private synchronized void handleTestStep(TestStepStarted event) {
                    if (event.getTestStep() instanceof PickleStepTestStep) {
                        log.info(DEVIDER);
                        log.info(
                                "Running STEP name: " + ((PickleStepTestStep) event.getTestStep()).getStepText());
                    }
                }
            };

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestStepStarted.class, stepHandler);
    }
}

package com.magento.assignment.steps;

import com.codeborne.selenide.Configuration;
import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.EventHandler;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.TestCaseStarted;
import io.cucumber.plugin.event.TestRunStarted;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LifecycleSteps implements ConcurrentEventListener {

    private static final String DIVIDER = "==============================";

    @Override
    public void setEventPublisher(EventPublisher eventPublisher) {
        eventPublisher.registerHandlerFor(TestRunStarted.class, setup);
        eventPublisher.registerHandlerFor(TestCaseStarted.class, before);
    }

    private final EventHandler<TestRunStarted> setup =
            event -> {
                Configuration.timeout = 20000;
                Configuration.pageLoadTimeout = 30000;
                Configuration.browserSize = "1920x1080";
                Configuration.browserPosition = "0x0";
                Configuration.pageLoadStrategy = "eager";
                Configuration.savePageSource = false;
                Configuration.browser = "chrome";
                Configuration.fastSetValue = false;
            };

    private final EventHandler<TestCaseStarted> before =
            event -> {
                log.info(DIVIDER);
                String scenarioName = event.getTestCase().getName();
                log.info("Running Scenario name: " + scenarioName);
            };
}

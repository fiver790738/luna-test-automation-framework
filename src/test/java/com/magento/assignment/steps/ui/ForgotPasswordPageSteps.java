package com.magento.assignment.steps.ui;

import com.magento.assignment.page.ForgotPasswordPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Value;

import static com.codeborne.selenide.Selenide.page;
import static com.magento.assignment.utils.Constants.*;

@Slf4j
public class ForgotPasswordPageSteps {

    @Value("${ui.user.email}")
    private String userEmail;

    private final ForgotPasswordPage forgotPasswordPage = page(ForgotPasswordPage.class);

    @Given("User verifies base elements on Forgot Password Page")
    public void verifyBaseElementsOnForgotPasswordPage() {
        log.info("Verifying forgot password page base elements...");

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(forgotPasswordPage.isPageTitleDisplayed()).isTrue();
        softly.assertThat(forgotPasswordPage.getPageTitleText()).isEqualTo("Forgot Your Password?");
        softly.assertThat(forgotPasswordPage.isNoteLabelDisplayed()).isTrue();
        softly.assertThat(forgotPasswordPage.getNoteLabelText())
                .isEqualTo("Please enter your email address below to receive a password reset link.");

        softly.assertThat(forgotPasswordPage.getEmailFieldLabelText()).isEqualTo("Email");
        softly.assertThat(forgotPasswordPage.isEmailInputFieldDisplayed()).isTrue();

        softly.assertThat(forgotPasswordPage.isResetPasswordButtonDisplayed()).isTrue();
        softly.assertThat(forgotPasswordPage.getResetPasswordButtonText()).isEqualTo("Reset My Password");

        softly.assertAll();
    }

    @When("User enters {string} value to email field on Forgot Password Page")
    public void enterValueToEmailField(String valueType) {
        log.info("Entering value to email field...");
        switch (valueType) {
            case VALID:
                forgotPasswordPage.enterValueToEmailInputField(userEmail);
                break;
            case INVALID:
                forgotPasswordPage.enterValueToEmailInputField("invalid_test_email");
                break;
            case EMPTY:
                break;
            default:
                throw new IllegalArgumentException(ERROR_MESSAGE);
        }
    }

    @When("User clicks on Reset Password button on Forgot Password Page")
    public void clickOnResetPasswordButton() {
        log.info("Clicking on Reset Password button...");
        forgotPasswordPage.clickOnResetPasswordButton();
    }

    @Then("User verifies {string} on Forgot Password Page")
    public void verifyErrorMessageLabelOnForgotPasswordPage(String errorMessage) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(forgotPasswordPage.isEmailFieldErrorLabelDisplayed()).isTrue();
        softly.assertThat(forgotPasswordPage.getEmailFieldErrorLabelText()).isEqualTo(errorMessage);
        softly.assertAll();
    }
}

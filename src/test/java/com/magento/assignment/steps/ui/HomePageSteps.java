package com.magento.assignment.steps.ui;

import com.codeborne.selenide.Selenide;
import com.magento.assignment.config.ContextConfig;
import com.magento.assignment.page.HomePage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import static com.codeborne.selenide.Selenide.page;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@ContextConfiguration(classes = ContextConfig.class)
public class HomePageSteps {

    @Value("${ui.base.url}")
    private String baseUrl;

    @Value("${ui.user.username}")
    private String username;

    private final HomePage homePage = page(HomePage.class);

    @Given("User opens home page")
    public void openHomePage() {
        log.info("Opening home page...");
        Selenide.open(baseUrl);
    }

    @When("User clicks on Sign In button on Home page")
    public void clickOnSignInButton() {
        log.info("Clicking on Sign In button on Home page...");
        homePage.clickOnSignInButton();
    }

    @Then("User is logged in")
    public void verifyThatUserIsLoggedIn() {
        log.info("Verifying that user is logged in...");
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(homePage.isUserLoggedInLabelDisplayed()).isTrue();
        softly.assertThat(homePage.getUserLoggedInLabelText()).isEqualTo(String.format("Welcome, %s!", username));
        softly.assertAll();
    }

    @When("User click on Sign Out button on Login page")
    public void clickOnSignOutButton() {
        log.info("Clicking on Sign Out button on Home page...");
        homePage.clickOnCustomerMenuButton();
        homePage.clickOnSignOutButton();
    }

    @Then("Sign in button is displayed on Home page")
    public void verifySignInButtonOnHomePage() {
        assertThat(homePage.isSignInButtonDisplayed()).isTrue();
    }
}
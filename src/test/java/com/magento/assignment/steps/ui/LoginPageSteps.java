package com.magento.assignment.steps.ui;

import com.magento.assignment.page.LoginPage;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.SoftAssertions;
import org.springframework.beans.factory.annotation.Value;

import static com.codeborne.selenide.Selenide.page;
import static com.magento.assignment.utils.Constants.*;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class LoginPageSteps {

    @Value("${ui.user.password}")
    private String userPassword;

    @Value("${ui.user.email}")
    private String userEmail;

    private final LoginPage loginPage = page(LoginPage.class);

    @Then("User verifies base elements on Login Page")
    public void verifyBaseElementsOnLoginPage() {
        log.info("Verifying login page base elements...");

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(loginPage.isPageTitleDisplayed()).isTrue();
        softly.assertThat(loginPage.getPageTitleText()).isEqualTo("Customer Login");
        softly.assertThat(loginPage.isLoginHeadingLabelDisplayed()).isTrue();
        softly.assertThat(loginPage.getLoginHeadingLabelText()).isEqualTo("Registered Customers");
        softly.assertThat(loginPage.isNoteLabelDisplayed()).isTrue();
        softly.assertThat(loginPage.getNoteLabelText())
                .isEqualTo("If you have an account, sign in with your email address.");

        softly.assertThat(loginPage.getEmailFieldLabelText()).isEqualTo("Email");
        softly.assertThat(loginPage.isEmailInputFieldDisplayed()).isTrue();
        softly.assertThat(loginPage.getPasswordFieldLabelText()).isEqualTo("Password");
        softly.assertThat(loginPage.isPasswordFieldLabelDisplayed()).isTrue();

        softly.assertThat(loginPage.isSignInButtonDisplayed()).isTrue();
        softly.assertThat(loginPage.getSignInButtonText()).isEqualTo("Sign In");
        softly.assertThat(loginPage.isForgotPasswordButtonDisplayed()).isTrue();
        softly.assertThat(loginPage.getForgotPasswordButtonText()).isEqualTo("Forgot Your Password?");

        softly.assertAll();
    }

    @When("User enters {string} value to email field")
    public void enterValueToEmailField(String valueType) {
        log.info("Entering value to email field...");
        switch (valueType) {
            case VALID:
                loginPage.enterValueToEmailInputField(userEmail);
                break;
            case INVALID:
                loginPage.enterValueToEmailInputField("invalid_test_email@gmail.com");
                break;
            case EMPTY:
                break;
            default:
                throw new IllegalArgumentException(ERROR_MESSAGE);
        }
    }

    @When("User enters {string} value to password field")
    public void enterValueToPasswordField(String valueType) {
        log.info("Entering value to password field...");
        switch (valueType) {
            case VALID:
                loginPage.enterValueToPasswordInputField(userPassword);
                break;
            case INVALID:
                loginPage.enterValueToPasswordInputField("invalid_test_password");
                break;
            case EMPTY:
                break;
            default:
                throw new IllegalArgumentException(ERROR_MESSAGE);
        }
    }

    @When("User clicks on Sign In button on Login page")
    public void clickOnSignInButtonOnLoginPage() {
        log.info("Clicking on Sign In button...");
        loginPage.clickOnSignInButton();
    }

    @Then("User verifies error message on Login page")
    public void verifyErrorMessageOnLoginPage() {
        log.info("Verifying error message...");
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(loginPage.isAlertLabelDisplayed()).isTrue();
        softly.assertThat(loginPage.getAlertLabelText(false)).isEqualTo(LOGIN_ERROR_MESSAGE);
        softly.assertAll();
    }

    @Then("User verifies required field error labels on Login page")
    public void verifyRequiredFieldsErrorLabelsOnLoginPage() {
        log.info("Verifying required fields error labels...");
        assertThat(loginPage.verifyRequiredFieldErrorLabels()).isTrue();
    }

    @When("User clicks on Forgot Password button on Login page")
    public void clickOnForgotPasswordButton() {
        log.info("Clicking on Forgot Password button...");
        loginPage.clickOnForgotPasswordButton();
    }


    @Then("User verifies success message on Login Page")
    public void verifySuccessMessageOnLoginPage() {
        log.info("Verifying success message...");
        assertThat(loginPage.getAlertLabelText(true))
                .isEqualTo(String.format(RESET_PASS_SUCCESS_MESSAGE, userEmail));
    }
}

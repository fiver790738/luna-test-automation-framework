package hooks;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

@Slf4j
public class Hook {

    @After
    public void teardown(Scenario scenario) {
        if (WebDriverRunner.hasWebDriverStarted())
            Selenide.closeWebDriver();

        if (scenario.isFailed()) {
            Optional<File> lastScreenshot = Screenshots.getLastThreadScreenshot();
            if (lastScreenshot.isPresent()) {
                byte[] screenshot;
                try {
                    screenshot = IOUtils.toByteArray(new FileInputStream(lastScreenshot.get()));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                scenario.embed(screenshot, "image/png");
                log.info("Screenshot has been added");
            } else {
                log.error("Screenshot can not be added");
            }
        }
    }
}

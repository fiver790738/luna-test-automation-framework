@WEB @UI @LoginPage
Feature: Login page feature

  Background:
    Given User opens home page
    When User clicks on Sign In button on Home page
    Then User verifies base elements on Login Page

  @HP_Login_1
  Scenario: Happy Path - Verification of success Customer login
    When User enters 'valid' value to email field
    And User enters 'valid' value to password field
    And User clicks on Sign In button on Login page
    Then User is logged in
    When User click on Sign Out button on Login page
    Then Sign in button is displayed on Home page

  @NP_Login_2
  Scenario Outline: Negative Path - Verification of unsuccessful Customer login with invalid credentials
    When User enters '<email>' value to email field
    And User enters '<password>' value to password field
    And User clicks on Sign In button on Login page
    Then User verifies error message on Login page

    Examples:
      | email   | password |
      | valid   | invalid  |
      | invalid | valid    |
      | invalid | invalid  |

  @NP_Login_3
  Scenario Outline: Negative Path - Verification of unsuccessful Customer login with empty credentials
    When User enters '<email>' value to email field
    And User enters '<password>' value to password field
    And User clicks on Sign In button on Login page
    Then User verifies required field error labels on Login page

    Examples:
      | email | password |
      | valid | empty    |
      | empty | valid    |
      | empty | empty    |

  @HP_Reset_Password_1
  Scenario: Happy Path - Verification of Reset Password with valid email
    When User clicks on Forgot Password button on Login page
    Then User verifies base elements on Forgot Password Page
    When User enters 'valid' value to email field on Forgot Password Page
    And User clicks on Reset Password button on Forgot Password Page
    Then User verifies success message on Login Page

  @NP_Reset_Password_2
  Scenario Outline: Negative Path - Verification of Reset Password with <email> email
    When User clicks on Forgot Password button on Login page
    Then User verifies base elements on Forgot Password Page
    When User enters '<email>' value to email field on Forgot Password Page
    And User clicks on Reset Password button on Forgot Password Page
    Then User verifies '<errorMessage>' on Forgot Password Page

    Examples:
      | email   | errorMessage                                                 |
      | empty   | This is a required field.                                    |
      | invalid | Please enter a valid email address (Ex: johndoe@domain.com). |
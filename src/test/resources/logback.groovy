import ch.qos.logback.classic.filter.ThresholdFilter
import groovy.json.JsonOutput
import net.logstash.logback.composite.GlobalCustomFieldsJsonProvider
import net.logstash.logback.composite.loggingevent.*
import net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder
import net.logstash.logback.stacktrace.ShortenedThrowableConverter

def environmentName = System.getenv("ENV_SHORT_NAME") != null ? System.getenv("ENV_SHORT_NAME") : "UNSET"
def groupId = System.getenv("APP_GROUP") != null ? System.getenv("APP_GROUP") : "GROUP"
def serviceName = System.getenv("SERVICE_NAME") != null ? System.getenv("SERVICE_NAME") : "SERVICE"

appender("CONSOLE", ConsoleAppender) {

    filter(ThresholdFilter) {
        level = INFO
    }

    encoder(LoggingEventCompositeJsonEncoder) {

        providers(LoggingEventJsonProviders) {

            timestamp(LoggingEventFormattedTimestampJsonProvider) {
                fieldName = '@apptimestamp'
                timeZone = 'UTC'
                pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            }

            logLevel(LogLevelJsonProvider)

            loggerName(LoggerNameJsonProvider) {
                fieldName = 'logger'
                shortenedLoggerNameLength = 35
            }

            message(MessageJsonProvider) {
                fieldName = 'message'
            }

            // NOTE: This is how to define log key=value pairs you want to be in every log entry.
            globalCustomFields(GlobalCustomFieldsJsonProvider) {
                customFields = "${JsonOutput.toJson("environmentName": environmentName, "groupId": groupId, "applicationId": serviceName)}"
            }

            threadName(ThreadNameJsonProvider) {
                fieldName = 'thread'
            }

            mdc(MdcJsonProvider) {}

            arguments(ArgumentsJsonProvider)

            // Exclude (probably unhelpful parts of stack trace.
            stackTrace(StackTraceJsonProvider) {
                throwableConverter(ShortenedThrowableConverter) {
                    maxDepthPerThrowable = 20
                    maxLength = 8192
                    shortenedClassNameLength = 35
                    exclude = /sun\..*/
                    exclude = /java\..*/
                    exclude = /groovy\..*/
                    exclude = /com\.sun\..*/
                    rootCauseFirst = true
                }
            }
        }
    }
}// json appender


root(INFO, ["CONSOLE"])
# Luna Test Automation Framework

Quality automation for Luna Web-site

## How to run automated tests locally
1. Create JUnit configuration for local run with the following parameters:
- java 11
- Environment variables: USER_PASSWORD=<user_password>
2. Open file TestRunner.java
3. OPTIONAL: change test tag in tags field, fox example: tags = {"@HP_Login_1"},
4. Run 'TestRunner'

### Note: 
Variables such as <user_password> contains in GitLab CI CD variables

## How to run automated tests on CI CD
1. Open repository page on GitLab
2. Select Build->Pipelines
3. Click on Run Pipeline button
4. Choose master branch and click on Run Pipeline button